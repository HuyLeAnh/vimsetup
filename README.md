# README #

This is my setup Vim editor by HuyLe: [https://bitbucket.org/HuyLeAnh/vimsetup](https://bitbucket.org/HuyLeAnh/vimsetup)

---
# CONFIGURE SETUP VIMRC-PLUGIN TO WORKING #
---
###1. Installer Vundle. [Refer at link](https://github.com/VundleVim/Vundle.vim)###
> * Setup Vundle
`$ git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`
`
> * Setup .vimrc file
`$ git clone https://HuyLeAnh@bitbucket.org/HuyLeAnh/vimsetup.git`
* Copy .vimrc file to `~/` $USER folder
* Open vim editor to excuting to install plugins:
>> * Launch vim and run `:PluginInstall`
>> * To install from command line: `vim +PluginInstall +qall`

Notes: Needing to install `vim-nox` to neocomplete runing
---